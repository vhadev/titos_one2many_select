odoo.define('titos_one2many_select.ListRenderer', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');

    ListRenderer.include({
        init: function (parent, state, params) {
            console.log(state, params);
            if (parent && parent.attrs) {
                this.selectable = parent.attrs.selectable;

                if (this.selectable) {
                    Object.assign(this.events, {
                        'click .titos_selectable': '_titos_onSelect',
                        'click .titos_select_all': '_titos_onSelectAll',
                        'click .o_titos_selectable_cell': '_titos_onSelectCell',
                        'click .o_titos_select_all_cell': '_titos_onSelectCell',
                    });
                }
            }

            this._super.apply(this, arguments);
        },

        _titos_onSelect: function (e) {
            e.stopPropagation();
            // var value = e.target.getAttribute('value');

            // if (!value) {
            //     return;
            // }

            // this.trigger_up('selectionchange', { value: value, checked: e.target.checked });
        },

        _titos_onSelectAll: function (e) {
            var checked = e.target.checked;
            var table = $(e.target).parents('table').get(0);

            table.querySelectorAll('.titos_selectable').forEach(node => node.checked = checked);
        },

        _titos_onSelectCell: function (e) {
            e.stopPropagation();
            var input = e.target.querySelector('input');

            if (!input) {
                return;
            }

            input.click();
        },

        _renderRow: function (record, index) {
            var $row = this._super.apply(this, arguments);

            // var isSection = record.data.display_type === 'line_section';
            // var isNote = record.data.display_type === 'line_note';

            // if (isSection || isNote) {}

            if (!this.selectable || !record) {
                return $row;
            }

            var $input = $('<input>', {
                type: 'checkbox',
                name: record.res_id,
                value: record.res_id,
                class: 'titos_selectable',
            });

            var $td = $('<td>', { class: 'o_titos_selectable_cell' }).append($input);
            $row.prepend($td);

            return $row;
        },

        _renderHeader: function () {
            var $thead = this._super.apply(this, arguments);

            if (!this.selectable) {
                return $thead;
            }

            var $input = $('<input>', {
                type: 'checkbox',
                name: 'select-all',
                value: -1,
                class: 'titos_select_all',
            });

            var $th = $('<th>', { class: 'o_titos_select_all_cell' }).append($input);

            $thead.children('tr').prepend($th);

            return $thead;
        }
    });
});    