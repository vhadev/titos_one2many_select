# -*- coding: utf-8 -*-
{
    "name": "Titos One2many Select",
    "version": "13.0.1.0",
    "license": "OPL-1",
    "website": "https://titosconsulting.com/",
    "summary": "Extends ListRenderer to accept a selectable attribute for one2many fields.",
    'description': "Extends ListRenderer to accept a selectable attribute for one2many fields.",
    "author": "TitosConsulting",
    "category": "web",
    "depends": ["web"],
    "data": [
        "views/assets.xml",
    ],
}
